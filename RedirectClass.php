<?php
namespace app\components\redirect;


use app\components\redirect\Location;
use SplFileObject;
use Yii;

class RedirectClass extends \yii\base\Component
{

    public $currentUrl = '';
    public $path = '';
    public $locations;


    public function __construct()
    {

        $this->currentUrl = Yii::$app->request->pathInfo;

        $this->path = $_SERVER['DOCUMENT_ROOT'] . Yii::$app->params['redirect_file'];

        $locations = $this->transform();

        $this->adapter($locations);

        $this->getResponse();
    }

    /**
     * @return object $location
     */
    public function getLocation()
    {

        $request = $this->currentUrl;

        return $this->find($request);
    }

    /**
     * Searches for a match of the request with the value of the property of the object location $before
     * @param $request
     * @return object $location or null
     */
    public function find($request)
    {
        foreach ($this->locations as $location) {
            if ($location->match($request)) {
                return $location;
            }
        }

        return null;
    }

    /**
     * Filters and forms an array of locations objects
     * @param $locations
     */
    public function adapter($locations)
    {
        $this->locations = array_map(function ($row) {
            $before = trim($row['before']);
            $after = trim($row['after']);
            $code = trim($row['code']);

            return new Location($before, $after, $code);
        }, $locations);
    }

    /**
     * We get an answer if we find a match with the request in the form of a redirect or return null
     * @return |null
     */
    public function getResponse()
    {
        $location = $this->getLocation();

        if ($location && Yii::$app->errorHandler->exception === null) {

            header("HTTP/1.1". $location->getCode() ."Moved Permanently");
            header("Location: " . $location->getAfter());
            exit();

        }

        return null;
    }

    /**
     *This action parses data from a file and transforms it into an array
     * @return array
     */
    protected function transform()
    {
        $path = $this->path;
        $file = new SplFileObject($path);
        $file->setFlags(SplFileObject::READ_AHEAD);
        $file->setFlags(SplFileObject::SKIP_EMPTY);
        $locations = [];

        while (!$file->eof()) {

            $location = $file->fgetcsv();
            if(is_array($location)) {
                if(!$location[0] == null){
                    $locations[] = $this->parse($location);

                }

            }

        }

        return array_filter($locations);

    }

    /**
     * This action parses the file line, which turned out in a loop and draws into an associative array
     * @param $row
     * @return array
     */
    protected function parse($row)
    {
        $result = array_filter($row);

        $before = $result[0];
        $after = $result[1];
        $code = $result[2];


        if ($before && $after && $code) {
            return [
                'before' => $before,
                'after' => $after,
                'code' => $code,
            ];
        }

        return [];
    }

}