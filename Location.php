<?php


namespace app\components\redirect;


use yii\web\Request;


class Location
{

    protected $before;
    protected $after;
    protected $code;

    public function __construct($before, $after, $code = 301)
    {
        $this->before = $before;
        $this->after = $after;
        $this->code = $code;
    }


    public function getBefore()
    {
        return $this->before;
    }


    public function getAfter()
    {
        return $this->after;
    }


    public function getCode()
    {
        return $this->code;
    }


    public function match($request)
    {
        $before = trim($this->getBefore(), '/');
        $pathInfo = trim($request, '/');

        return fnmatch($before, $pathInfo);
    }
}